import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
Vue.use(VueRouter)

const routes = [

    {
        path: '/',
        alias: '/comandes',
        name: 'comandes',
        meta: { requiresAuth: true },
        component: () =>
            import ('../views/Comandes.vue')
    },
    {
        path: '/login',
        alias: ['/register', '/login'],
        name: 'Auth',
        component: () =>
            import ('../views/Auth.vue')
    },

    {
        path: '/*',
        name: 'Error',
        component: () =>
            import ('../views/Error.vue')
    },

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in 
        // if not, redirect to login page. 
        if (store.getters.getUserName == '') {
            next(
                '/login'

            )
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()! 
    }
})
export default router