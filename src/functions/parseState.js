const states = [
    {
        value:0 , text: 'En preparación'
    },
    {
        value:1 , text: 'Preparado'
    },
    {
        value:2 , text: 'En ruta'
    },
    {
        value:3 , text: 'Entregado'
    },
    {
        value:4 , text: 'Cliente ausente'
    },
    {
        value:5 , text: 'Domicilio equivocado'
    },
    {
        value:6 , text: 'No se ha tenido el tiempo suficiente'
    },
    {
        value:7 , text: 'Cliente fallecido'
    },
    
]
const parseState = (stateNumber)=>{
    return states.find((state)=>state.value == stateNumber).text
}
export default parseState