import Vue from "vue";
import Vuex from "vuex";
import router from "../router/index";
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        logguedUser: {
            created_at: "",
            email: "",
            id: "",
            name: "",
            rol: 0,
            updated_at: "",
        },
        orders: [],
        modal: {
            id: 0,
            active: false
        },
        notificacion: {
            active: false,
            type: "",
            text: "",
        },
    },
    mutations: {
        setloguedUser(state, user) {
            try {
                state.logguedUser = user;
                return { success: true, msg: "usuario almacenado ", errorCode: 0 };
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        setOrders(state, orders) {
            try {
                state.orders = orders;
                return { success: true, msg: "ordenes almacenadas ", errorCode: 0 };
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 6 };
            }
        },
        changeModalStatus(state) {

            state.modal.active = !state.modal.active;
            return { success: true, msg: "estado del modal cambiado", errorCode: 0 };

        },
        setOrderIdOnModal(state, orderId) {

            state.modal.id = orderId;
            return { success: true, msg: "el id del modal ha cambiado", errorCode: 0 };

        },
        setNotificacion(state, notificacion) {
            try {
                state.notificacion = notificacion;
                setTimeout(() => {
                    state.notificacion = {};
                }, 5000);
                return { success: true, msg: "modal activado", errorCode: 0 };
            } catch (error) {
                return { success: false, msg: "error activando modal", errorCode: 7 };
            }
        },
    },
    getters: {
        getUserName(state) {
            return state.logguedUser.name;
        },
        getModalStatus(state) {
            return state.modal.active;
        },
        getActualModalOrderId(state) {
            return state.modal.id;
        },
        getNotShippedOrders(state) {
            let orders = state.orders.reduce((total, order) => {
                if (order.state < 2) total.push(order);
                return total;
            }, []);
            return orders;
        },
        getShippedOrders(state) {
            let orders = state.orders.reduce((total, order) => {
                if (order.state > 1) total.push(order);
                return total;
            }, []);
            return orders;
        },
        getNotificacion(state) {
            return state.notificacion
        },
    },
    actions: {
        async getLoggedUser(context) {
            try {
                let response = await Vue.axios.get("/api/user");
                console.log('asd')
                if (response.data.rol == 'dealer' || response.data.rol == 'admin') {
                    context.commit("setloguedUser", response.data);

                    return {
                        success: true,
                        msg: "guardados datos de logueo",
                        errorCode: 0,
                    };
                }

                return {
                    success: false,
                    msg: "error al guardar datos de logueo",
                    errorCode: 4,
                };


            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        async getOrders(context) {
            try {
                let response = await Vue.axios.get("/api/ordersapi");
                if (!response.data) {
                    return {
                        success: false,
                        msg: "error al guardar las ordenes",
                        errorCode: 4,
                    };
                }

                context.commit("setOrders", response.data);
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        async login(context, userCredential) {
            try {
                let response = await Vue.axios.post("/api/loginapi", userCredential);
                if (!response.data.token)
                    return { success: false, msg: "error al loguearse", errorCode: 3 };
                Vue.$cookies.set("Authorization", `Bearer ${response.data.token}`);
                let user = await context.dispatch("getLoggedUser");
                console.log(user)
                if (user.success) {
                    router.push("/comandes");
                    return { success: true, msg: "logueo correcto", errorCode: 0 };

                } else {
                    return { success: false, msg: "error al loguearse", errorCode: 3 };

                }



            } catch (error) {
                return { success: false, msg: "error al loguearse", errorCode: 3 };
            }
        },

        async register(context, userCredential) {
            try {
                userCredential.rol = 'dealer'
                let response = await Vue.axios.post("/api/registerapi", userCredential);
                if (!response.data.name)
                    return { success: false, msg: "error al registrarse", errorCode: 2 };
                const user = {
                    email: userCredential.email,
                    password: userCredential.password,
                };
                context.dispatch("login", user);
                return { success: true, msg: "registrado correctamente", errorCode: 0 };
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        async putChangeOrderState(context, { order, stateNumber }) {
            try {
                let editedOrder = JSON.parse(JSON.stringify(order));
                editedOrder.state = Number(stateNumber)
                let response = await Vue.axios.put(`/api/ordersapi/${editedOrder.id}`, editedOrder);
                if (!response.data) return { success: false, msg: "error al editar el estado de la orden", errorCode: 3 };
                context.dispatch('getOrders')

            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };

            }
        },
        async openOrderModal(context, orderId) {
            context.commit('setOrderIdOnModal', orderId)
            context.commit('changeModalStatus')
        },

    },
    modules: {},
});