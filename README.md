<a name="item0"></a>
# APLICACIÓN REPARTIDOR


## ÍNDICE DE CONTENIDOS

* [APLICACIÓN EMPRESA](#item0)
* [CREDENCIALES PARA CONECTARSE](#item1)
* [PUESTA EN MARCHA](#item2)
* [ENLACE AL SITIO WEB](item7)
* [DESCRIPCIÓN](#item3)
    * [PÁGINA PRINCIPAL](#item4)
* [DESPLEGAMIENTO](item5)
* [RESULTADO FINAL](item6)

<a name="item1"></a>
## CREDENCIALES PARA CONECTARSE

**CONECTARSE A LA WEB:**   
**USUARIO**: dealer@dealer.com   
**CONTRASEÑA** : dealer

**CONECTARSE MEDIANTE SSH:**   
**USUARIOS:** ubuntu(), reparto(1234)

<a name="item2"></a>
## PUESTA EN MARCHA
Instalación previa node JS
Instalación previa de GIT
Instalación previa de vue-cli-service y vue 2.x.   

**FUNCIONAMIENTO EN LOCAL:** 

```sh
git clone https://gitlab.com/Isra-gonza/aplicacion-repartidor.git   

npm install   

npm run serve
```

**PRODUCCIÓN:**

Cuando el desarrollo esté comlpetado simplemente ejecutaremos el script.sh situado en la base del proyecto:

```sh
sh script.sh
```

<a name="item7"></a>
## ENLACE AL SITIO WEB

Enlace a la página del [repartidor][url]

<a name="item3"></a>
## DESCRIPCIÓN

<a name="item4"></a>
### PÁGINA PRINCIPAL

**BLOQUE SUPERIOR**

Órdenes en ruta seleccionadas por el repartidor para repartir.
Marcar como entregado
Marcar como no entregado, junto con el motivo.


**BLOQUE INFERIOR**

Órdenes en preparación seleccionables.
Marcar como Preparado.
Marcar como en ruta

<a name="item5"></a>
## DESPLEGAMIENTO

Ejecución del `script.sh`
<a name="item6"></a>
## RESULTADO FINAL

![](https://media.discordapp.net/attachments/808372312986222645/812667740963340308/unknown.png?width=1295&height=660)

[VOLVER](#item0)

[url]: <http://reparto.g05.batoilogic.es/comandes>
